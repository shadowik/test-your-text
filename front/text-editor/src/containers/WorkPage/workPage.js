import React, {Component} from 'react'
import {Redirect, Switch, BrowserRouter as Router, Route} from 'react-router-dom'
import './workPage.css'
import axios from 'axios'
import image1 from '../../images/1.png'
import image2 from '../../images/2.png'
import image3 from '../../images/3.png'
import image4 from '../../images/4.png'
import image5 from '../../images/5.png'
import image6 from '../../images/6.png'
import image7 from '../../images/7.png'
import image8 from '../../images/8.png'

class workPage extends Component {

  constructor (props) {
    super(props)
    this.state = {
      isDark: props.isDark,
      sendButtonValue: "send",
      text: "",
      buttonValue: "send",
      postText: "",
      textValue: "",
      result: null,
      menuOfSinonymsIsOpen: 0, 
      synonymsArray: [],
      changeToWord: "",
      wordShoulBeChanged: 0,
      indexOfSelectedWord: -1,
      data: [],
      newSynonim: "",
      numberOfSymbols: 0,
      loadingProces: 0,
      massage: ""
    }
    this.changeText = this.changeText.bind(this)
    this.createText = this.createText.bind(this)
  }
  

  changeText(event) {
    let stroke = event.target.value
    this.setState({text: stroke, textValue: stroke})
  }


  handleSubmit = event => {
    event.preventDefault()
    const text = {
      text: this.state.text
    }

    if (this.state.loadingProces === 0)
    {
      this.setState({loadingProces: 1})
    }

    axios.post(`http://d908-176-116-140-45.ngrok.io/`, { text })
      .then(res => {
        console.log(res)
        console.log(res.data)
        console.log(this.state.text)
        this.setState({
          data: res.data.arrayOfWords,
          synonymsArray: [],
          loadingProces: 0,
          numberOfSymbols: res.data.simCount,
          massage: res.data.error
        })
        console.log(this.state.data)
      })
  }

  createValueOfTextArea(event) {
    console.log(event.target.value)
  }

  openSynonymList(synonyms) {
    console.log(synonyms[0])
  }


  createText() {

    if (this.state.loadingProces === 1) {
      return (
      <div className = "loader"> 
        <div className = "lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
      </div>)
    }

    let data = this.state.data
    const wordIndex = this.state.indexOfSelectedWord
    if (this.state.wordShoulBeChanged === 1) {
      data[wordIndex].word = this.state.changeToWord
      data[wordIndex].hasChanged = 1
      this.setState({wordShoulBeChanged: 0, data: data})
    }
    
    const text = (<div className = "TextTransformed">
      <div className = "moveText">
        {
        data.map((element, index) => {
          let word = element.word
          let word2 = word
          const synonimId = this.state.changeId
          if (synonimId > -1 && this.state.indexOfSelectedWord === index) {
            let synonyms = element.synonyms
            word2 = synonyms[synonimId]
          }
          if (element.itIsN === 1) {
            return (<br />)
          }
          let result = (<a key = {index} className = "word_theUsualStyle">{word}</a>)
          if (index === 0) {
            return(result)
          }
          else if (element.itIsWord === 1) {
            if (element.len_of_syn > 0) {
              let wordStyle = "word_withSynonims synonim"
              if (element.hasChanged === 1) {
                wordStyle += " isChanged"
              }
              let synonyms = element.synonyms
              result = (
                <a key = {index} className = {wordStyle} onClick = {() => { this.setState({synonymsArray: synonyms, indexOfSelectedWord: index})}}>
                  {word}
                </a> 
              )
            }
            return(result)
          }
          else {
            return(result)
          }
        })
      }
      </div> 
      
    </div>)
  
    return text
  }

  handleCreateSynonim() {
  }
  handleChangeSynonim(event) {
    console.log(event.target.value)
    this.setState({newSynonim: event.target.value})
  }

  isDark (light) {
    return (this.state.isDark === true ? "Dark": "Light")
  }

  render() {
    
    console.log()
    let light = this.isDark(this.state.isDark)
    const cls = [
      'SendText',
      "SendText" + light
    ]

    let workPage = "workPage"
    workPage += " workPage" + light

    let workPageBody = "workPageBody"
    workPageBody += " workPageBody" + light

    let workPageFooter = "workPageFooter"
    workPageFooter += " workPageFooter" + light

    return (
      <div className = {workPage}>
        <div className = {workPageBody}>
          <div className = "aboutProgramFunctions"> 
            <div className = "aboutProgramFunctions_images"> 
              <div className = "aboutProgramFunctions_imagePosition_3">
                <img src = {image3} className = "image3" />
              </div>
              <div className = "aboutProgramFunctions_imagePosition_1">
                <img src = {image1} className = "image1" />
              </div>
              <div className = "aboutProgramFunctions_imagePosition_2">
                <img src = {image2} className = "image2" />
              </div>
              <div className = "aboutProgramFunctions_imagePosition_4">
                <img src = {image4} className = "image4" />
              </div>
            </div>
            <div className = {"aboutProgramFunctions_text " + ("aboutProgramFunctions_text" + light)}> 
              <a>Сервис позволяет проверять текст на тавтологию, лично выбирать подходящие слова из списка предлагаемых синонимов.</a>
            </div>
          </div>
          <div className = "textPart"> 
            <div className = "enterText">
                <form className = "enterTextForm" onSubmit = {this.handleSubmit}>
                    <textarea className = "TextArea" key = "TextArea" onChange = {this.changeText}/>
                    <input className = {cls.join(' ')} type="submit" value="проверить" disabled = {this.state.loadingProces}/>
                </form>
                <div className = {"textPart_parametrs " + "textPart_parametrs" + light}> 
                  <a>Количество символов: {this.state.numberOfSymbols}</a><br />
                </div>
            </div>
            <div className = "ResultPart">
              <div className = "ResultPartForm"> 
                {this.createText()}
                <div className = "SynonimPart"> 
                  {this.state.synonymsArray.map((synonim, index) => {
                    return(
                      <a key = {index} className = "synonimOut" onClick = {() => {this.setState({changeToWord: synonim, wordShoulBeChanged: 1})}}> 
                        {synonim + " "}
                      </a>)
                  })}
                </div>
              </div>
              <div className = {"ResultPartMessage ResultPartMessage" + light}> 
                {this.state.massage}
              </div>
            </div>
          </div>
          
        </div>
        <div className = {workPageFooter}>
          <div className = {"icon icon" + light}>
            <a href = "https://vk.com/shadowik">
              {
                light === "Dark" ?
                  (<img className = "vkImageDark" src={image6} alt="Пример" />)
                  :
                  (<img className = "vkImageLight" src={image5} alt="Пример" />)
              }
            </a>
            
          </div>
          <div className = {"switchLight switchLight" + light}>
            <a href = {"http://ca8c-176-116-140-45.ngrok.io/slide=1&theme=" + (light === "Dark" ? "light" : "dark")}>
              {
                light === "Dark" ?
                  (<img className = "lightImageDark" src={image7} alt="Пример" />)
                  :
                  (<img className = "lightImageLight" src={image8} alt="Пример" />)
              }
            </a>
          </div>
            
          
        </div>
        
      </div>

    )
  }
}

export default workPage

/*


<a>Количество слов: 
                    {() => {
                      let data = this.state.data
                      let numberOfWords = 0
                      data.map((e, index) => {
                        numberOfWords += e.itIsWord
                      })
                      return numberOfWords
                    }}
                  </a>


/*onClick = {() => {
                        console.log(111)
                        
                        if(this.state.areaTextValueShouldBeClear === 1) {
                          this.setState({areaTextValueShouldBeClear: 0, areaTextValue: ""})
                        }
                      }
                    }*/


 /*{() => {
                        let result = ""
                        if (this.state.areaTextValueShouldBeClear === 1)
                        {
                          result = (<span>Вставьте текст до 5000 символов...</span>)
                        }
                        return(result)
                      }}
            
            


<div className = "bottomOfBody"> 

          </div>

  createText() {
    const data = this.state.data
    const text = []
    data.map((element, index) => {
      console.log("index = " + index)
      console.log("word = " + element.word)
      const word = element.word
      if (index === 0) {
        text.push(<p className = "word_theUsualStyle">{word}</p>)
      }
      else if (element.itIsWord !== 1) {
        const wordStyle = "word_theUsualStyle"
        if (element.len_of_syn > 0) {
          wordStyle = "word_withSynonims"
        }
        text.push(<p className = {wordStyle}>{word}</p>)
      }
      else {
        text.push(<p className = "word_theUsualStyle">{word}</p>)
      }
      
    })
    console.log("text = " + text)
    return (<div>{text.join()}</div>)
  }



  <div className = "SynonimForm">
                <div className = "SynonimCreate">
                <form className = "newSynonim" onSubmit = {this.handleCreateSynonim}>
                    <textarea className = "newSynonim_textArea" key = "newSynonim" onChange = {this.handleChangeSynonim}/>
                    <input className = "newSynonim_button" type="submit" value="Добавить"/>
                </form>
                </div>
                <div className = "SynonimPart"> 
                  {this.state.synonymsArray.map((synonim, index) => {
                    return(
                      <a key = {index} className = "synonimOut" onClick = {() => {this.setState({changeToWord: synonim, wordShoulBeChanged: 1})}}> 
                        {synonim + " "}
                      </a>)
                  })}
                </div>
              </div>
              
*/