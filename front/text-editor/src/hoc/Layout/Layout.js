import React, {Component} from 'react'
import WorkPage from '../../containers/WorkPage/workPage'
import './Layout.css'
import {
    Switch,
    Route, 
    Redirect,
    useLocation
  } from 'react-router-dom'
import { BrowserRouter as Router } from 'react-router-dom'
import axios from 'axios'



class Layout extends Component {


    constructor (props) {
        super(props)
        this.state = {
        }
    }

    componentDidMount() {
        /*axios.get('datare.json', 
            {
                headers : { 
                'Content-Type': 'application/json',
                'Accept': 'application/json'
                }
            })
            .then((res) => {
                this.setState({header: res.data.header, body: res.data.body, lists: [10]});
                this.addCposiss()
                this.getCposis()
                this.buildNumbers()
                this.columnDate()
            }
            )
            .catch((err) => console.error(err))*/
    }

    render() {
        return (
            <div className = "Layout">
                <Router>
                    <Switch>
                        <Route exact path={`/slide=1&theme=light`} component={ (()=>(<WorkPage isDark = {false}/>)) } />
                        <Route exact path={`/slide=1&theme=dark`} component={ (()=>(<WorkPage isDark = {true}/>)) } />
                        
                    </Switch>
                </Router>
            </div>
        )
    }
}

export default Layout