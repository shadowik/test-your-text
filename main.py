# -*- coding: utf-8 -*-

from flask import Flask, render_template, redirect, request, jsonify
from flask_restful import Api, Resource
from flask_cors import CORS
from nlp.gensim_model_synonyms import GensimModel
from algoritms.main import GenerateMainText
from forms.mainForm import MainForm


app = Flask(__name__)
app.config['SECRET_KEY'] = 'my_project_key'
api = Api(app)
CORS(app)


class SynonymsResource(Resource):
    def get(self):
        json_data = request.json
        print(json_data)
        word = json_data.get('word', None)
        count = json_data.get('count', None)
        if word is None:
            return jsonify({'error': 'Field \'word\' not found'})
        if count is None:
            return jsonify({'error': 'Field \'count\' not found'})
        synonyms = gensim_model.get_synonyms(word, count)

        print('[INFO] Got post request ->', word, count)

        return jsonify({'synonyms': synonyms})


@app.route('/', methods=['post', 'get'])
def start_page():
    f = request.get_json()
    print(f['text']['text'])
    lst = GenerateMainText(f['text']['text'])
    print(lst)
    dct = {'arrayOfWords': [], 'simCount': len(f['text']['text'].replace('\n', '')), 'error': 'Слишком много символов' if lst[0] else ''}
    lst = lst[1:]
    for i in lst:
        if i[0] + i[-1] == '()':
            m = i[1:-1].split('|')
            dct['arrayOfWords'].append(
                {'word': m[0],
                 'hasChanged': 0,
                 'idProblem': m[1],
                 'itIsWord': 1,
                 'itIsSimbol': 0,
                 'itIsN': 0,
                 'itIsSpace': 0,
                 'taftology': 1,
                 'len_of_syn': len([m[0]] + m[2:]),
                 'synonyms': [m[0]] + m[2:]})
        else:
            if i.isalpha():
                dct['arrayOfWords'].append(
                    {'word': i,
                     'itIsWord': 1,
                     'idProblem': 0,
                     'hasChanged': 0,
                     'itIsSimbol': 0,
                     'itIsN': 0,
                     'itIsSpace': 0,
                     'taftology': 0,
                     'len_of_syn': 0,
                     'synonyms': []})
            elif i == ' ':
                dct['arrayOfWords'].append(
                    {'word': i,
                     'itIsWord': 0,
                     'idProblem': 0,
                     'hasChanged': 0,
                     'itIsSimbol': 0,
                     'itIsN': 0,
                     'itIsSpace': 1,
                     'taftology': 0,
                     'len_of_syn': 0,
                     'synonyms': []})
            else:
                if i == '–':
                    dct['arrayOfWords'].append(
                        {'word': ' –',
                         'itIsWord': 0,
                         'idProblem': 0,
                         'hasChanged': 0,
                         'itIsSimbol': 1,
                         'itIsN': 0,
                         'itIsSpace': 0,
                         'taftology': 0,
                         'len_of_syn': 0,
                         'synonyms': []})
                elif i == '\n':
                    dct['arrayOfWords'].append(
                        {'word': '\n',
                         'itIsWord': 0,
                         'idProblem': 0,
                         'hasChanged': 0,
                         'itIsSimbol': 0,
                         'itIsN': 1,
                         'itIsSpace': 0,
                         'taftology': 0,
                         'len_of_syn': 0,
                         'synonyms': []})
                else:
                    dct['arrayOfWords'].append(
                        {'word': i,
                         'itIsWord': 0,
                         'idProblem': 0,
                         'hasChanged': 0,
                         'itIsSimbol': 1,
                         'itIsN': 0,
                         'itIsSpace': 0,
                         'taftology': 0,
                         'len_of_syn': 0,
                         'synonyms': []})
    print(dct)
    f = None
    lst = None
    return dct


api.add_resource(SynonymsResource, '/api/synonyms')


print('[INFO] Loading model...')
gensim_model = GensimModel('nlp/ruwikiruscorpora_upos_cbow_300_20_2017.bin.gz')
print('[INFO] Model loaded successfully!')


if __name__ == '__main__':
    app.run(port=8000, host='127.0.0.1')
