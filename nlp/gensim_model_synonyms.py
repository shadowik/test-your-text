from typing import List
import pymorphy2
import gensim

PYMORPHY_TO_GENSIM_TAGS = {
    'ADJF': 'ADJ',
    'ADJS': 'ADJ',
    'ADVB': 'ADV',
    'COMP': 'ADV',
    'GRND': 'VERB',
    'INFN': 'VERB',
    'NOUN': 'NOUN',
    'PRED': 'ADV',
    'PRTF': 'ADJ',
    'PRTS': 'VERB',
    'VERB': 'VERB'
}


class SynonymizerGensimModelIsNotLoaded(Exception):
    pass


class GensimModel:
    def __init__(self, filename: str):
        self._model = gensim.models.KeyedVectors. \
            load_word2vec_format(filename, binary=True)
        self._model.fill_norms()

        self._morph = pymorphy2.MorphAnalyzer()

    def get_model(self):
        return self._model

    def get_synonyms(self, word: str, max_count: int) -> List[str]:
        if self._model is None:
            SynonymizerGensimModelIsNotLoaded()
        word_parsed = self._morph.parse(word)[0]
        if word_parsed.tag.POS not in PYMORPHY_TO_GENSIM_TAGS:
            return ['']
        word_gensim_string = \
            word_parsed.normalized.word + '_' + PYMORPHY_TO_GENSIM_TAGS[word_parsed.tag.POS]
        if word_gensim_string not in self._model:
            return ['']
        synonyms_variants = self._model.most_similar([word_gensim_string], topn=max_count)
        if not synonyms_variants:
            return ['']
        synonyms_variants = \
            list(filter(lambda x: x[0].split('_')[1] == PYMORPHY_TO_GENSIM_TAGS[word_parsed.tag.POS],
                        synonyms_variants))
        synonyms_variants = synonyms_variants[:max_count]

        synonyms_variants = list(map(lambda x: x[0].split('_')[0], synonyms_variants))

        return synonyms_variants


if __name__ == '__main__':
    model = GensimModel('ruwikiruscorpora_upos_cbow_300_20_2017.bin.gz')

    print(model.get_synonyms('красивый', 10))
