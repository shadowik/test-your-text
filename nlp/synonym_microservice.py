from flask import Flask, jsonify, request
from flask_restful import Api, Resource
from gensim_model_synonyms import GensimModel


class SynonymsResource(Resource):
    def get(self):
        json_data = request.json
        print(json_data)
        word = json_data.get('word', None)
        count = json_data.get('count', None)
        if word is None:
            return jsonify({'error': 'Field \'word\' not found'})
        if count is None:
            return jsonify({'error': 'Field \'count\' not found'})
        synonyms = gensim_model.get_synonyms(word, count)

        print('[INFO] Got post request ->', word, count)

        return jsonify({'synonyms': synonyms})


app = Flask(__name__)
api = Api(app)

api.add_resource(SynonymsResource, '/api/synonyms')


print('[INFO] Loading model...')

gensim_model = GensimModel('../nlp/ruwikiruscorpora_upos_cbow_300_20_2017.bin.gz')

print('[INFO] Model loaded successfully!')

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=88)

from requests import get

# print(get('http://127.0.0.1:88/api/synonyms', json={'word': 'зеркальный', 'count': 10}).json())
