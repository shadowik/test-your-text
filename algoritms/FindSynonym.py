import requests


def synonym(word, type='noun,adjective,verb,adverb', length=100):
    key = '6fa0ebea-32e9-488b-a823-c86aecce4207'
    url = 'https://api.wordassociations.net/associations/v1.0/json/search?'
    text = word
    response = requests.get(url, params={'apikey': key, 'text': text, 'lang': 'ru', 'pos': type})

    j = response.json()
    if len(j['response'][0]['items']) < length:
        length = len(j['response'][0]['items'])
    return [j['response'][0]['items'][i]['item'] for i in range(length)]


if __name__ == '__main__':
    print(synonym(input(), 'noun'))