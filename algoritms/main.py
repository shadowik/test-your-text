# -- coding: utf-8 --
import re
import time
from pprint import pprint

import pymorphy2
import gensim
from fuzzywuzzy import fuzz
from statistics import mean
from requests import get
from fuzzywuzzy import process
from nlp import gensim_model_synonyms as GenMod
from nltk.stem.snowball import SnowballStemmer


#model = GenMod.GensimModel('../nlp/ruwikiruscorpora_upos_cbow_300_20_2017.bin.gz')
stemmer = SnowballStemmer("russian")
morph = pymorphy2.MorphAnalyzer()


# string = input()

W1 = 1
W2 = 1
W3 = 1
W_MAX = 63


def StrAnalyze(s1, s2):
    if len(s1) < len(s2):
        s1, s2 = s2, s1

    for i in range(len(s1) - 1, 0, -1):
        for j in range(0, len(s1) - i):
            s = s1[j:j + i]
            if s2.count(s) != 0:
                return i * 100 / (len(s1) - 1)
    return 0



PUNCTUATION = '.,;:-\\/()?!+=\'\"'
RUSSIAN_ALPHABET = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя-"


def GenerateMainText(text):
    words = {"": 1}
    words_reference = {"":-1}
    tmp = ""
    text = text.replace("\n"," \n ")
    res = []
    m = []
    if(len(text)>5100):
        res.append(True)
        text = text[:5100]
    else:
        res.append(False)
    for word in text.split(" "):
        if word == "":
            continue
        if word[-1].isalpha() and word[0].isalpha():
            m.append(word)
        else:
            if word[0] == "'" or word[0] == '"' or word[0] == "(":
                m.append(word[0])
                word = word[1:]
            if word[0].isalpha() and not word[-1].isalpha():
                t = len(word) - 1
                while not word[t].isalpha():
                    t -= 1
                t += 1
                m.append(word[:t])
                m.append(word[t:])
            else:
                m.append(word)



    PosDict = {
        "NOUN": "noun",
        "ADJF": "adjective",
        "ADJS": "adjective",
        "VERB": "verb",
        "INFN": "verb",
        "ADVB": "adverb"
    }

    now_sent = 0
    space_count =0
    for i in range(len(m)):
        s = m[i]
        if(s=="\n"):
            res.append("\n")
            continue
        if (s == ".") or s == "!" or s == "?" or s == "...":
            now_sent += 1
            if now_sent - 3 >= 0:
                for w_key in list(words.keys()):
                    if words[w_key] == now_sent - 3:
                        del words[w_key]

        if s[0].isalpha() and len(s) > 2 and all([symbol.lower() in RUSSIAN_ALPHABET for symbol in s]):

            s = s.lower()

            SNF = morph.parse(s)[0].normal_form
            SNO = stemmer.stem(s)

            for v in (list(words.keys())):
                NNF = v
                NNO = stemmer.stem(v)
                P1 = fuzz.WRatio(SNF, NNF) * W1
                P2 = fuzz.WRatio(SNO, NNO) * W2
                P3 = StrAnalyze(SNF, NNF) * W3
                average_value = (P1 + P2 + P3) / 3
                print(s + " и " + v + " похожи " + str(P1) + " " + str(P2) + " " + str(
                    P3) + " основной =" + str(average_value))
                if average_value > W_MAX:

                    if __name__ == '__main__':
                        print(s + " и " + v + " похожи " + str(P1) + " " + str(P2) + " " + str(
                        P3) + " основной =" + str(average_value))

                    opc_tags = morph.parse(s)[0].tag

                    part_of_speech = opc_tags.POS
                    t_number = opc_tags.number
                    t_gender = opc_tags.gender
                    t_tense = opc_tags.tense
                    t_aspect = opc_tags.aspect
                    t_case = opc_tags.case

                    try:
                        #synonyms = FoundS.synonym(s, type=PosDict[part_of_speech])
                        flag = True
                        while(flag):
                            try:
                                synonyms = get('http://127.0.0.1:8000/api/synonyms', json={'word': s, 'count': 20}).json()["synonyms"]
                                #synonyms = get('http://127.0.0.1:88/api/synonyms',json={'word': s, 'count': 20}).json()["synonyms"]
                                flag = False
                            except Exception:
                                pass
                        #synonyms = model.get_synonyms(s,20)
                    except KeyError:
                        continue
                    temp_result = []
                    temp_result.append(m[i]+"|"+str(words_reference[v]))

                    sims_count = 0
                    for Si in synonyms:
                        if(Si.find(":")!=-1):
                            continue
                        sims_count += 1
                        current_word = Si
                        NNF = Si
                        NNO = stemmer.stem(Si)
                        P1 = fuzz.WRatio(SNF, NNF) * W1
                        P2 = fuzz.WRatio(SNO, NNO) * W2
                        P3 = StrAnalyze(SNF, NNF) * W3
                        average_value = (P1 + P2 + P3) / 3

                        if (average_value > W_MAX):
                            continue

                        Stmp = ChangeTag(morph.parse(current_word)[0].tag, opc_tags)

                        transform_res = morph.parse(current_word)[0].inflect(Stmp)
                        if(not transform_res is None):
                            current_word = transform_res.word

                        else:
                            continue
                        if(m[i][0].isupper()) and current_word:
                            current_word = current_word[0].upper()+current_word[1:]
                        temp_result.append(str(current_word))
                        if len(temp_result) >= min(5,len(synonyms)):
                            sims_count = -1
                            break

                    if(len(temp_result)==2 and temp_result[1]==""):
                        res.append("(" + temp_result[0] + ")")
                        break
                    res.append("("+"|".join(temp_result)+")")

                    break
            else:
                res.append(m[i])
            words[s] = now_sent
            words_reference[s] = i+space_count
        else:
            res.append(m[i])
        try:
            if m[i][-1].isalpha() and m[i + 1][0] in PUNCTUATION:
                continue
        except IndexError:
            pass
        try:
            if(m[i+1]=="\n"):
                continue
        except Exception:
            pass

        space_count+=1
        res.append(" ")


    return res

def ChangeTag(tags,NTags):
    res = set()

    if(tags.POS == "INFN" or tags.POS == "VERB"):
        #res.add("VERB")
        if(NTags.tense!=None):
            res.add(NTags.tense)
            if (NTags.tense == "past"):
                if (NTags.gender != None):
                    res.add(NTags.gender)
        if (NTags.mood != None):
            res.add(NTags.mood)
        if (NTags.person != None):
            res.add(NTags.person)
        if (NTags.number != None):
            res.add(NTags.number)

        return res
    if(tags.POS == "ADJF" or tags.POS == "ADJS"):
        if (NTags.number != None):
            res.add(NTags.number)
        if (NTags.gender != None):
            res.add(NTags.gender)
        if (NTags.case != None):
            res.add(NTags.case)
        return res
    if(tags.POS == "NOUN"):
        if (NTags.number != None):
            res.add(NTags.number)
        if (NTags.case != None):
            res.add(NTags.case)
        return res
    return res







# testing_text = 'Текст, текстовый "текст" что-то: это - он... (предложение или текст)' \
#                ' прыгающий прыгающий приехать шла пришёл прибыть прибыть лучник мученик'


# testing_text = "Однокоренные слова – слова, имеющие одинаковый корень, объединяющий все лексемы в один" \
#       " смысловой ряд. Так как корень является главной частью слова, передающей основное" \
#       " лексическое значение, то слова с одним корнем схожи по смыслу, однако точное значение" \
#        " немного различается."
#
# testing_text = "Около нашей деревни есть небольшой лесок. Летом ребята собирают там ягоды. За грибами мы" \
#        " ходим в большой лес за рекой. Там живет лесник. Хорошо отдохнуть в его лесном домике"
# testing_text = "Электромонтажная схема - это чертеж, на котором показано расположение деталей и соединение их между" \
#                " собой проводами. Отдельные детали на монтажной схеме не обозначаются условными знаками, а " \
#                "изображаются так, как они примерно выглядят (подробности конструкции обычно не показывают)."
# testing_text = "Подбирать однокоренные слова — полезный навык, чтобы грамотно писать. Например, можно проверить," \
#                " какая гласная пишется в безударной позиции в корне, если подобрать родственное слово, где эта " \
#                "гласная будет под ударением. Также через подбор однокоренных слов можно проверить написание согласных" \
#                " в корне."
# testing_text = "Цветок, однако, не хотел жить печально; поэтому, когда ему бывало совсем горестно, он дремал. Всё " \
#                "же он постоянно старался расти. В середине лета цветок распустил венчик сверху. До этого он был" \
#                " похож на травку, а теперь стал настоящим цветком. Венчик у него был составлен из лепестков простого " \
#                "светлого цвета, ясного и сильного, как у звезды. И как звезда, он светился живым мерцающим огнём, и его" \
#                " видно было даже в тёмную ночь. А когда ветер приходил на пустырь, он всегда касался цветка и уносил " \
#                "его запах с собою."
# testing_text = "Ах, не говорите мне про Австрию! Я ничего не понимаю, может быть, но Австрия никогда не хотела и не" \
#                " хочет войны. Она предает нас. Россия одна должна быть спасительницей Европы. Наш благодетель знает" \
#                " свое высокое призвание и будет верен ему. Вот одно, во что я верю. Нашему доброму и чудному" \
#                " государю предстоит величайшая роль в мире, и он так добродетелен и хорош, что Бог не оставит" \
#                " его, и он исполнит свое призвание задавить гидру революции, которая теперь еще ужаснее в лице" \
#                " этого убийцы и злодея. Мы одни должны искупить кровь праведника. На кого нам надеяться, я вас " \
#                "спрашиваю?.. Англия с своим коммерческим духом не поймет и не может понять всю высоту души " \
#                "императора Александра. Она отказалась очистить Мальту. Она хочет видеть, ищет заднюю мысль наших " \
#                "действий. Что они сказали Новосильцеву? Ничего. Они не поняли, они не могли понять самоотвержения " \
#                "нашего императора, который ничего не хочет для себя и все хочет для блага мира. И что они обещали? " \
#                "Ничего. И что обещали, и того не будет! Пруссия уже объявила, что Бонапарте непобедим и что вся Европа" \
#                " ничего не может против него... И я не верю ни в одном слове ни Гарденбергу, ни Гаугвицу.  Я верю в" \
#                " одного Бога и в высокую судьбу нашего милого императора. Он спасет Европу!.. — Она вдруг " \
#                "остановилась с улыбкой насмешки над своею горячностью."

# testing_text = "В жизни нам часто приходится работать с данными, основу которых составляют числа. Но иногда весь объем" \
#                " данных, с которыми мы работаем, может быть настолько большой, что человеку становиться трудно работать" \
#                " с ним. Тут нам и помогают компьютеры с их мощными вычислительными и запоминающими способностями." \
#                " Но сам по себе компьютер не сможет ничего сделать с нашими данными, ведь он не знает, как с ними" \
#                " обращаться и что нам необходимо с ними сделать. Для этого люди создали языки программирования" \
#                " и типизацию данных. С помощью языков программирования (далее - ЯП) мы можем написать программу" \
#                " – последовательность команд, действий, понятных компьютеру, тем самым объяснив ему, что мы хотим" \
#                " получить в итоге. А с помощью типов данных мы можем рассказать компьютеру, как и где необходимо" \
#                " хранить, передавать, изменять и считывать наши данные.Вот, например, такой простой тип данных," \
#                " как числа. Мы знаем, как записываются числа, как они складываются, умножаются и т.д. Компьютер " \
#                "же этого, изначально не знает и для того, чтобы он смог работать с целыми числами, люди придумали " \
#                "целочисленный тип данных integer [2, с. 38]. А для того, чтобы хранить сразу много чисел и удобно " \
#                "оперировать ими существует такой тип данных, как массив. В массиве можно хранить множество значений" \
#                " одного типа данных и обращаться к отдельно взятому элементу массива по его индексу [2, с. 59]." \
#                " Массивы есть во всех ЯП, в том числе и в ЯП Python, который я буду использовать для написания" \
#                " примеров программ и алгоритмов. Однако в Python вместо массивов чаще всего используется другая" \
#                " структура данных – список. В списке, в отличие от массива, могут храниться объекты разных типов" \
#                " данных, в том числе и числа [3, с. 166]. Так как в этой работе мы не будем иметь дело со списками," \
#                " элементы которых представлены разными типами данных, то далее списки в Python мы будем тоже называть" \
#                " массивами."

#testing_text = "использование предобученной дистрибутивно - семантической дистрибутивно  - семантической модели для поиска синонимов"

testing_text = "Люблю тебя любовь моя"



if __name__ == '__main__':
    import FindSynonym as FoundS

    start_time = time.time()
    pprint(GenerateMainText(testing_text))
    print("--- %s seconds ---" % (time.time() - start_time))
else:
    from . import FindSynonym as FoundS




    # start_time = time.time()
    # for a in testing_text.split(" "):
    #     if a == "":
    #         continue
    #     if a[-1].isalpha() and a[0].isalpha():
    #         m.append(a)
    #     else:
    #         if a[0] == "'" or a[0] == '"' or a[0] == "(":
    #             m.append(a[0])
    #             a = a[1:]
    #         if a[0].isalpha() and not a[-1].isalpha():
    #             t = len(a) - 1
    #             while not a[t].isalpha():
    #                 t -= 1
    #             t += 1
    #             m.append(a[:t])
    #             m.append(a[t:])
    #         else:
    #             m.append(a)
    #
    # res = []
    #
    # PosDict = {
    #     "NOUN": "noun",
    #     "ADJF": "adjective",
    #     "ADJS": "adjective",
    #     "VERB": "verb",
    #     "INFN": "verb",
    #     "ADVB": "adverb"
    # }
    #
    # now_sent = 0
    # for i in range(len(m)):
    #     s = m[i]
    #     if (s == ".") or s == "!" or s == "?" or s == "...":
    #         now_sent += 1
    #         if now_sent - 3 >= 0:
    #             for w_key in list(words.keys()):
    #                 if words[w_key] == now_sent - 3:
    #                     del words[w_key]
    #
    #     if s[0].isalpha() and len(s) > 2:
    #
    #         s = s.lower()
    #
    #         SNF = morph.parse(s)[0].normal_form
    #         SNO = stemmer.stem(s)
    #
    #         for v in (list(words.keys())):
    #             NNF = v
    #             NNO = stemmer.stem(v)
    #             P1 = fuzz.WRatio(SNF, NNF) * W1
    #             P2 = fuzz.WRatio(SNO, NNO) * W2
    #             P3 = StrAnalyze(SNF, NNF) * W3
    #             average_value = (P1 + P2 + P3) / 3
    #             if average_value > W_MAX:
    #                 time.sleep(0.1)
    #
    #                 print(s + " и " + v + " похожи " + str(P1) + " " + str(P2) + " " + str(
    #                     P3) + " основной =" + str(average_value))
    #
    #                 opc_tags = morph.parse(s)[0].tag
    #
    #                 part_of_speech = opc_tags.POS
    #                 t_number = opc_tags.number
    #                 t_gender = opc_tags.gender
    #                 t_tense = opc_tags.tense
    #
    #                 t_aspect = opc_tags.aspect
    #                 t_case = opc_tags.case
    #                 t_mood = opc_tags.mood
    #                 t_person = opc_tags.person
    #
    #
    #                 try:
    #                     synonyms = FoundS.synonym(s, type=PosDict[part_of_speech])
    #                 except KeyError:
    #                     continue
    #                 temp_result = ""
    #                 temp_result += "(" + s + "|"
    #                 sims_count = 0
    #                 max_count = min(len(synonyms),5)
    #                 for Si in synonyms:
    #
    #                     sims_count += 1
    #                     current_word = Si
    #                     Stmp = ChangeTag(morph.parse(current_word)[0].tag,opc_tags)
    #                     print(Stmp,current_word)
    #                     current_word = morph.parse(current_word)[0].inflect(Stmp).word
    #                     print(current_word)
    #
    #                     temp_result += str(current_word)
    #                     if sims_count >= max_count:
    #                         sims_count = -1
    #                         break
    #                     else:
    #                         temp_result += "|"
    #                 temp_result += ")"
    #                 res.append(temp_result)
    #                 break
    #         else:
    #             res.append(m[i])
    #         words[s] = now_sent
    #     else:
    #         res.append(m[i])
    #     try:
    #         if m[i][-1].isalpha() and not m[i + 1][0].isalpha():
    #             continue
    #     except IndexError:
    #         pass
    #
    #     res.append(" ")
    # print(words)
    # print(res)
    # print("\n")
    # print("--- %s seconds ---" % (time.time() - start_time))

    # for a in list(words.keys()):
    #     for b in list(words.keys()):
    #         n2 = fuzz.WRatio(a,b)
    #         n1 = StrAnalyze(a,b)*11
    #         srz = int((n1+n2)/12)
    #         if(srz>=44 and n2!=100):
    #             print(str(a)+" и "+str(b)+" = "+str(srz)+" " + str(n2)+" "+str(n1/8))
